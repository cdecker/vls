//! By convention, structs ending with `Def` are serde local types
//! describing how to serialize a remote type via `serde(remote)`.
//! Structs ending with `Entry` are local types that require a manual
//! transformation from the remote type - implemented via `From` / `Into`.

use lightning_signer::prelude::*;

use alloc::borrow::Cow;
use core::fmt;
use core::fmt::Formatter;
use core::time::Duration;

use bitcoin::hashes::Hash;
use bitcoin::secp256k1::PublicKey;
use bitcoin::{OutPoint, Script, Txid};
use lightning::ln::chan_utils::ChannelPublicKeys;
use lightning::ln::PaymentHash;
use lightning::util::ser::Writer;
use lightning_signer::chain::tracker::ListenSlot;
use lightning_signer::{io, lightning};
use serde::de::SeqAccess;
use serde::ser::SerializeSeq;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde_with::serde_as;
use serde_with::{DeserializeAs, SerializeAs};

use lightning_signer::bitcoin;
use lightning_signer::bitcoin::secp256k1::ecdsa::Signature;
use lightning_signer::channel::{ChannelId, ChannelSetup, CommitmentType};
use lightning_signer::monitor::State as ChainMonitorState;
use lightning_signer::node::{PaymentState, PaymentType};
use lightning_signer::policy::validator::{CommitmentSignatures, EnforcementState};
use lightning_signer::tx::tx::{CommitmentInfo2, HTLCInfo2};

#[derive(Copy, Clone, Debug, Default)]
pub struct PublicKeyHandler;

impl SerializeAs<PublicKey> for PublicKeyHandler {
    fn serialize_as<S>(source: &PublicKey, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(hex::encode(source.serialize().to_vec()).as_str())
    }
}

impl<'de> DeserializeAs<'de, PublicKey> for PublicKeyHandler {
    fn deserialize_as<D>(deserializer: D) -> Result<PublicKey, D::Error>
    where
        D: Deserializer<'de>,
    {
        let res = <Cow<'de, str> as Deserialize<'de>>::deserialize(deserializer).unwrap();
        let key = PublicKey::from_slice(hex::decode(&*res).unwrap().as_slice()).unwrap();
        Ok(key)
    }
}

pub struct ChannelIdHandler;

impl SerializeAs<ChannelId> for ChannelIdHandler {
    fn serialize_as<S>(source: &ChannelId, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(hex::encode(source.as_slice()).as_str())
    }
}

impl<'de> DeserializeAs<'de, ChannelId> for ChannelIdHandler {
    fn deserialize_as<D>(deserializer: D) -> Result<ChannelId, D::Error>
    where
        D: Deserializer<'de>,
    {
        let res = <Cow<'de, str> as Deserialize<'de>>::deserialize(deserializer).unwrap();
        let key = ChannelId::new(&hex::decode(&*res).unwrap());
        Ok(key)
    }
}

#[serde_as]
#[derive(Serialize, Deserialize)]
#[serde(remote = "ChannelPublicKeys")]
pub struct ChannelPublicKeysDef {
    #[serde_as(as = "PublicKeyHandler")]
    pub funding_pubkey: PublicKey,
    #[serde_as(as = "PublicKeyHandler")]
    pub revocation_basepoint: PublicKey,
    #[serde_as(as = "PublicKeyHandler")]
    pub payment_point: PublicKey,
    #[serde_as(as = "PublicKeyHandler")]
    pub delayed_payment_basepoint: PublicKey,
    #[serde_as(as = "PublicKeyHandler")]
    pub htlc_basepoint: PublicKey,
}

#[derive(Deserialize)]
struct ChannelPublicKeysHelper(#[serde(with = "ChannelPublicKeysDef")] ChannelPublicKeys);

impl SerializeAs<ChannelPublicKeys> for ChannelPublicKeysDef {
    fn serialize_as<S>(value: &ChannelPublicKeys, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        ChannelPublicKeysDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, ChannelPublicKeys> for ChannelPublicKeysDef {
    fn deserialize_as<D>(
        deserializer: D,
    ) -> Result<ChannelPublicKeys, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        ChannelPublicKeysHelper::deserialize(deserializer).map(|h| h.0)
    }
}

pub struct VecWriter(pub Vec<u8>);
impl Writer for VecWriter {
    fn write_all(&mut self, buf: &[u8]) -> Result<(), io::Error> {
        self.0.extend_from_slice(buf);
        Ok(())
    }
}

struct TxidDef;

impl SerializeAs<Txid> for TxidDef {
    fn serialize_as<S>(value: &Txid, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(hex::encode(value.to_vec()).as_str())
    }
}

impl<'de> DeserializeAs<'de, Txid> for TxidDef {
    fn deserialize_as<D>(deserializer: D) -> Result<Txid, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        let res = <Cow<'de, str> as Deserialize<'de>>::deserialize(deserializer).unwrap();
        let txid = Txid::from_slice(hex::decode(&*res).unwrap().as_slice()).unwrap();
        Ok(txid)
    }
}

#[serde_as]
#[derive(Serialize, Deserialize)]
#[serde(remote = "OutPoint")]
pub struct OutPointDef {
    #[serde_as(as = "TxidDef")]
    pub txid: Txid,
    pub vout: u32,
}

#[derive(Deserialize)]
struct OutPointHelper(#[serde(with = "OutPointDef")] OutPoint);

impl SerializeAs<OutPoint> for OutPointDef {
    fn serialize_as<S>(value: &OutPoint, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        OutPointDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, OutPoint> for OutPointDef {
    fn deserialize_as<D>(deserializer: D) -> Result<OutPoint, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        OutPointHelper::deserialize(deserializer).map(|h| h.0)
    }
}

#[derive(Serialize, Deserialize)]
#[serde(remote = "CommitmentType")]
pub enum CommitmentTypeDef {
    Legacy,
    StaticRemoteKey,
    Anchors,
    AnchorsZeroFeeHtlc,
}

#[derive(Deserialize)]
struct CommitmentTypeHelper(#[serde(with = "CommitmentTypeDef")] CommitmentType);

impl SerializeAs<CommitmentType> for CommitmentTypeDef {
    fn serialize_as<S>(value: &CommitmentType, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        CommitmentTypeDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, CommitmentType> for CommitmentTypeDef {
    fn deserialize_as<D>(deserializer: D) -> Result<CommitmentType, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        CommitmentTypeHelper::deserialize(deserializer).map(|h| h.0)
    }
}

#[derive(Serialize, Deserialize)]
#[serde(remote = "Script")]
pub struct ScriptDef(#[serde(getter = "Script::to_bytes")] Vec<u8>);

impl From<ScriptDef> for Script {
    fn from(s: ScriptDef) -> Self {
        Script::from(s.0)
    }
}

#[derive(Deserialize)]
struct ScriptHelper(#[serde(with = "ScriptDef")] Script);

impl SerializeAs<Script> for ScriptDef {
    fn serialize_as<S>(value: &Script, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        ScriptDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, Script> for ScriptDef {
    fn deserialize_as<D>(deserializer: D) -> Result<Script, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        ScriptHelper::deserialize(deserializer).map(|h| h.0)
    }
}

#[serde_as]
#[derive(Serialize, Deserialize)]
#[serde(remote = "ChannelSetup")]
pub struct ChannelSetupDef {
    pub is_outbound: bool,
    pub channel_value_sat: u64,
    pub push_value_msat: u64,
    #[serde_as(as = "OutPointDef")]
    pub funding_outpoint: OutPoint,
    pub holder_selected_contest_delay: u16,
    #[serde_as(as = "Option<ScriptDef>")]
    pub holder_shutdown_script: Option<Script>,
    #[serde(with = "ChannelPublicKeysDef")]
    pub counterparty_points: ChannelPublicKeys,
    pub counterparty_selected_contest_delay: u16,
    #[serde_as(as = "Option<ScriptDef>")]
    pub counterparty_shutdown_script: Option<Script>,
    #[serde_as(as = "CommitmentTypeDef")]
    pub commitment_type: CommitmentType,
}

#[derive(Deserialize)]
struct ChannelSetupHelper(#[serde(with = "ChannelSetupDef")] ChannelSetup);

impl SerializeAs<ChannelSetup> for ChannelSetupDef {
    fn serialize_as<S>(value: &ChannelSetup, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        ChannelSetupDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, ChannelSetup> for ChannelSetupDef {
    fn deserialize_as<D>(deserializer: D) -> Result<ChannelSetup, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        ChannelSetupHelper::deserialize(deserializer).map(|h| h.0)
    }
}

#[serde_as]
#[derive(Serialize, Deserialize)]
#[serde(remote = "PaymentHash")]
pub struct PaymentHashDef(pub [u8; 32]);

#[derive(Deserialize)]
struct PaymentHashHelper(#[serde(with = "PaymentHashDef")] PaymentHash);

impl SerializeAs<PaymentHash> for PaymentHashDef {
    fn serialize_as<S>(value: &PaymentHash, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        PaymentHashDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, PaymentHash> for PaymentHashDef {
    fn deserialize_as<D>(deserializer: D) -> Result<PaymentHash, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        PaymentHashHelper::deserialize(deserializer).map(|h| h.0)
    }
}

#[serde_as]
#[derive(Serialize, Deserialize)]
#[serde(remote = "HTLCInfo2")]
pub struct HTLCInfo2Def {
    pub value_sat: u64,
    #[serde_as(as = "PaymentHashDef")]
    pub payment_hash: PaymentHash,
    pub cltv_expiry: u32,
}

#[derive(Deserialize)]
struct HTLCInfo2Helper(#[serde(with = "HTLCInfo2Def")] HTLCInfo2);

impl SerializeAs<HTLCInfo2> for HTLCInfo2Def {
    fn serialize_as<S>(value: &HTLCInfo2, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        HTLCInfo2Def::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, HTLCInfo2> for HTLCInfo2Def {
    fn deserialize_as<D>(deserializer: D) -> Result<HTLCInfo2, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        HTLCInfo2Helper::deserialize(deserializer).map(|h| h.0)
    }
}

#[serde_as]
#[derive(Serialize, Deserialize)]
#[serde(remote = "CommitmentInfo2")]
pub struct CommitmentInfo2Def {
    pub is_counterparty_broadcaster: bool,
    pub to_countersigner_pubkey: PublicKey,
    pub to_countersigner_value_sat: u64,
    pub revocation_pubkey: PublicKey,
    pub to_broadcaster_delayed_pubkey: PublicKey,
    pub to_broadcaster_value_sat: u64,
    pub to_self_delay: u16,
    #[serde_as(as = "Vec<HTLCInfo2Def>")]
    pub offered_htlcs: Vec<HTLCInfo2>,
    #[serde_as(as = "Vec<HTLCInfo2Def>")]
    pub received_htlcs: Vec<HTLCInfo2>,
    #[serde(default)] // TODO remove default once everybody upgraded
    pub feerate_per_kw: u32,
}

#[derive(Deserialize)]
struct CommitmentInfo2Helper(#[serde(with = "CommitmentInfo2Def")] CommitmentInfo2);

impl SerializeAs<CommitmentInfo2> for CommitmentInfo2Def {
    fn serialize_as<S>(value: &CommitmentInfo2, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        CommitmentInfo2Def::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, CommitmentInfo2> for CommitmentInfo2Def {
    fn deserialize_as<D>(
        deserializer: D,
    ) -> Result<CommitmentInfo2, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        CommitmentInfo2Helper::deserialize(deserializer).map(|h| h.0)
    }
}

#[serde_as]
#[derive(Serialize, Deserialize)]
#[serde(remote = "CommitmentSignatures")]
pub struct CommitmentSignaturesDef(pub Signature, pub Vec<Signature>);

#[derive(Deserialize)]
struct CommitmentSignaturesHelper(#[serde(with = "CommitmentSignaturesDef")] CommitmentSignatures);

impl SerializeAs<CommitmentSignatures> for CommitmentSignaturesDef {
    fn serialize_as<S>(value: &CommitmentSignatures, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        CommitmentSignaturesDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, CommitmentSignatures> for CommitmentSignaturesDef {
    fn deserialize_as<D>(
        deserializer: D,
    ) -> Result<CommitmentSignatures, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        CommitmentSignaturesHelper::deserialize(deserializer).map(|h| h.0)
    }
}

#[serde_as]
#[derive(Serialize, Deserialize)]
#[serde(remote = "EnforcementState")]
pub struct EnforcementStateDef {
    pub next_holder_commit_num: u64,
    pub next_counterparty_commit_num: u64,
    pub next_counterparty_revoke_num: u64,
    pub current_counterparty_point: Option<PublicKey>,
    pub previous_counterparty_point: Option<PublicKey>,
    #[serde_as(as = "Option<CommitmentInfo2Def>")]
    pub current_holder_commit_info: Option<CommitmentInfo2>,
    #[serde_as(as = "Option<CommitmentSignaturesDef>")]
    pub current_counterparty_signatures: Option<CommitmentSignatures>,
    #[serde_as(as = "Option<CommitmentInfo2Def>")]
    pub current_counterparty_commit_info: Option<CommitmentInfo2>,
    #[serde_as(as = "Option<CommitmentInfo2Def>")]
    pub previous_counterparty_commit_info: Option<CommitmentInfo2>,
    pub channel_closed: bool,
    #[serde(default)] // TODO remove default once everyone upgrades
    pub initial_holder_value: u64,
}

#[derive(Deserialize)]
struct EnforcementStateHelper(#[serde(with = "EnforcementStateDef")] EnforcementState);

impl SerializeAs<EnforcementState> for EnforcementStateDef {
    fn serialize_as<S>(value: &EnforcementState, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        EnforcementStateDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, EnforcementState> for EnforcementStateDef {
    fn deserialize_as<D>(
        deserializer: D,
    ) -> Result<EnforcementState, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        EnforcementStateHelper::deserialize(deserializer).map(|h| h.0)
    }
}

#[serde_as]
#[derive(Serialize, Deserialize, Debug)]
#[serde(remote = "ListenSlot")]
pub struct ListenSlotDef {
    #[serde_as(as = "OrderedSet<TxidDef>")]
    pub txid_watches: OrderedSet<Txid>,
    #[serde_as(as = "OrderedSet<OutPointDef>")]
    watches: OrderedSet<OutPoint>,
    #[serde_as(as = "OrderedSet<OutPointDef>")]
    seen: OrderedSet<OutPoint>,
}

#[derive(Deserialize)]
struct ListenSlotHelper(#[serde(with = "ListenSlotDef")] ListenSlot);

impl SerializeAs<ListenSlot> for ListenSlotDef {
    fn serialize_as<S>(value: &ListenSlot, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        ListenSlotDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, ListenSlot> for ListenSlotDef {
    fn deserialize_as<D>(deserializer: D) -> Result<ListenSlot, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        ListenSlotHelper::deserialize(deserializer).map(|h| h.0)
    }
}

#[serde_as]
#[derive(Serialize, Deserialize, Debug)]
#[serde(remote = "ChainMonitorState")]
pub struct ChainMonitorStateDef {
    height: u32,
    funding_txids: Vec<Txid>,
    funding_vouts: Vec<u32>,
    funding_inputs: OrderedSet<OutPoint>,
    funding_height: Option<u32>,
    funding_outpoint: Option<OutPoint>,
    funding_double_spent_height: Option<u32>,
    closing_height: Option<u32>,
}

#[derive(Deserialize)]
struct ChainMonitorStateHelper(#[serde(with = "ChainMonitorStateDef")] ChainMonitorState);

impl SerializeAs<ChainMonitorState> for ChainMonitorStateDef {
    fn serialize_as<S>(value: &ChainMonitorState, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        ChainMonitorStateDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, ChainMonitorState> for ChainMonitorStateDef {
    fn deserialize_as<D>(
        deserializer: D,
    ) -> Result<ChainMonitorState, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        ChainMonitorStateHelper::deserialize(deserializer).map(|h| h.0)
    }
}

#[derive(Copy, Clone, Debug, Default)]
pub struct DurationHandler;

impl SerializeAs<Duration> for DurationHandler {
    fn serialize_as<S>(value: &Duration, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(2))?;
        seq.serialize_element(&value.as_secs())?;
        seq.serialize_element(&value.subsec_nanos())?;
        seq.end()
    }
}

struct DurationVisitor;

impl<'de> serde::de::Visitor<'de> for DurationVisitor {
    type Value = Duration;

    fn expecting(&self, fmt: &mut Formatter) -> fmt::Result {
        fmt.write_str("tuple")
    }

    fn visit_seq<V>(self, mut seq: V) -> Result<Duration, V::Error>
    where
        V: SeqAccess<'de>,
    {
        let secs = seq.next_element()?.ok_or_else(|| serde::de::Error::invalid_length(0, &self))?;
        let nanos =
            seq.next_element()?.ok_or_else(|| serde::de::Error::invalid_length(1, &self))?;
        Ok(Duration::new(secs, nanos))
    }
}

impl<'de> DeserializeAs<'de, Duration> for DurationHandler {
    fn deserialize_as<D>(deserializer: D) -> Result<Duration, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_seq(DurationVisitor)
    }
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(remote = "PaymentType")]
pub enum PaymentTypeDef {
    Invoice,
    Keysend,
}

#[derive(Deserialize)]
struct PaymentTypeHelper(#[serde(with = "PaymentTypeDef")] PaymentType);

impl SerializeAs<PaymentType> for PaymentTypeDef {
    fn serialize_as<S>(value: &PaymentType, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        PaymentTypeDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, PaymentType> for PaymentTypeDef {
    fn deserialize_as<D>(deserializer: D) -> Result<PaymentType, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        PaymentTypeHelper::deserialize(deserializer).map(|h| h.0)
    }
}

#[serde_as]
#[derive(Serialize, Deserialize, Debug)]
#[serde(remote = "PaymentState")]
pub struct PaymentStateDef {
    pub invoice_hash: [u8; 32],
    pub amount_msat: u64,
    pub payee: PublicKey,
    #[serde_as(as = "DurationHandler")]
    pub duration_since_epoch: Duration,
    #[serde_as(as = "DurationHandler")]
    pub expiry_duration: Duration,
    pub is_fulfilled: bool,
    #[serde_as(as = "PaymentTypeDef")]
    pub payment_type: PaymentType,
}

#[derive(Deserialize)]
struct PaymentStateHelper(#[serde(with = "PaymentStateDef")] PaymentState);

impl SerializeAs<PaymentState> for PaymentStateDef {
    fn serialize_as<S>(value: &PaymentState, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        PaymentStateDef::serialize(value, serializer)
    }
}

impl<'de> DeserializeAs<'de, PaymentState> for PaymentStateDef {
    fn deserialize_as<D>(deserializer: D) -> Result<PaymentState, <D as Deserializer<'de>>::Error>
    where
        D: Deserializer<'de>,
    {
        PaymentStateHelper::deserialize(deserializer).map(|h| h.0)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::model::ChainTrackerEntry;
    use bitcoin::blockdata::constants::genesis_block;
    use bitcoin::Network;
    use core::iter::FromIterator;
    use lightning_signer::bitcoin::TxMerkleNode;
    use lightning_signer::chain::tracker::{ChainTracker, Error};
    use lightning_signer::monitor::ChainMonitor;
    use lightning_signer::util::test_utils::*;

    #[test]
    fn test_chain_tracker() -> Result<(), Error> {
        let tx = make_tx(vec![make_txin(1), make_txin(2)]);
        let outpoint = OutPoint::new(tx.txid(), 0);
        let monitor = ChainMonitor::new(outpoint, 0);
        monitor.add_funding(&tx, 0);
        let genesis = genesis_block(Network::Regtest);
        let mut tracker = ChainTracker::new(Network::Regtest, 0, genesis.header)?;
        tracker.add_listener(monitor.clone(), OrderedSet::new());
        let header = make_header(tracker.tip(), TxMerkleNode::all_zeros());
        tracker.add_block(header, vec![], None)?;
        tracker.add_listener_watches(
            monitor,
            OrderedSet::from_iter(vec![make_txin(1).previous_output]),
        );

        let entry = ChainTrackerEntry::from(&tracker);
        let json = serde_json::to_string(&entry).expect("json");
        let entry_de: ChainTrackerEntry = serde_json::from_str(&json).expect("de json");
        let _tracker_de: ChainTracker<ChainMonitor> = entry_de.into();
        Ok(())
    }
}
